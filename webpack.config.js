const path = require('path');
const slsw = require('serverless-webpack');
const nodeExternals = require('webpack-node-externals');
const specificDeps = require('./package.json').serviceSpecificDependencies;

module.exports = {
  mode: slsw.lib.webpack.isLocal ? 'development' : 'production',
  entry: slsw.lib.entries,
  devtool: 'source-map',
  resolve: {
    extensions: ['.js', '.json', '.ts'],
  },
  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, '.webpack'),
    filename: '[name].js',
  },
  target: 'node',
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      { test: /\.ts$/, loader: 'ts-loader' },
    ],
  },
  externals: [
    nodeExternals({
      whitelist: specificDeps || [],
      modulesFromFile: true, // search in package.json instead of node_modules folder
    }),
  ],
  node: {
    __dirname: false,
  },
  // currently disabled due to typeorm
  // cannot deal minified code
  optimization: {
    minimize: false,
  },
};
