import { SecretsManager } from 'aws-sdk';

const { SECRETS_MANAGER_KEY: SecretId, AWS_REGION: region } = process.env;
const secretManager = new SecretsManager({ region });

export default async (): Promise<{}> => {
  if (!SecretId || !region) {
    throw new Error('Required env variables were not provided');
  }
  const secrets: SecretsManager.GetSecretValueResponse = await secretManager.getSecretValue({ SecretId }).promise();
  try {
    return JSON.parse(secrets as string);
  } catch (error) {
    return secrets;
  }
};
