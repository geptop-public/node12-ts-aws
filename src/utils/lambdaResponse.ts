import { getStatusText, INTERNAL_SERVER_ERROR, OK } from 'http-status-codes';

interface ErrorBody {
  error: {
    name: string;
    message: string;
  };
  status?: number;
}

const DEFAULT_RESPONSE_HEADERS = {
  'content-type': 'application/json',
  'Access-Control-Allow-Headers': 'Content-Type,x_client_device_id,x_client_id,x-sr-token,x-endpoint-version',
  'Access-Control-Allow-Methods': 'OPTIONS,GET,POST,PUT,DELETE',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Expose-Headers':
    'Access-Control-Allow-Origin,Access-Control-Allow-Methods,X-JSESSIONID,x-sr-token-valid',
};

export const lambdaSuccessResponse = (
  data: string | object = getStatusText(OK),
  statusCode = OK,
  additionalHeaders = {},
): object => ({
  statusCode,
  headers: { ...DEFAULT_RESPONSE_HEADERS, ...additionalHeaders },
  body: JSON.stringify(data, null, 2),
});

export const lambdaErrorResponse = (error: Error): object => {
  const statusCode = INTERNAL_SERVER_ERROR;
  const body: ErrorBody = {
    error: { name: error.name, message: error.message },
  };
  // if (err instanceof InvalidSchemaError) {
  //   statusCode = 400;
  // }
  body.status = statusCode;

  return {
    statusCode,
    headers: DEFAULT_RESPONSE_HEADERS,
    body: JSON.stringify(body),
  };
};
