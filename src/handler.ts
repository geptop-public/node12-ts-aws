import { Handler } from 'aws-lambda';
import log from 'lambda-log';
import { lambdaSuccessResponse, lambdaErrorResponse } from './utils';
import 'source-map-support/register';

export const hello: Handler = async (event): Promise<{}> => {
  try {
    log.info('Starting lambda');
    return lambdaSuccessResponse({
      message: 'Go Serverless Webpack (Typescript) v1.0! Your function executed successfully!',
      event,
    });
  } catch (error) {
    return lambdaErrorResponse(error);
  }
};
